package cern.ais.pm.command;

import java.math.BigDecimal;
import java.math.BigInteger;

public class AccountCommand {

    private BigDecimal amount;
    private BigInteger from;
    private BigInteger to;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigInteger getFrom() {
        return from;
    }

    public void setFrom(BigInteger from) {
        this.from = from;
    }

    public BigInteger getTo() {
        return to;
    }

    public void setTo(BigInteger to) {
        this.to = to;
    }
}
