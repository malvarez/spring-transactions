package cern.ais.pm;

import cern.ais.pm.domain.Account;
import cern.ais.pm.domain.Person;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.stream.Stream;

@SpringBootApplication
@EnableTransactionManagement
public class TransactionApplication implements ApplicationListener<ContextRefreshedEvent> {

    private final EntityManager entityManager;

    public TransactionApplication(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        Long personCount = entityManager
                .createQuery("select count(p) from Person p", Long.class)
                .getSingleResult();
        if (personCount == 0) {
            Stream.of(
                    new Person("Batman", new Account(new BigDecimal("1000"))),
                    new Person("Superman", new Account(BigDecimal.ZERO)),
                    new Person("Spiderman", new Account(BigDecimal.ZERO))
            ).forEach(entityManager::persist);
        }
    }

    public static void main(final String[] args) {
        SpringApplication.run(TransactionApplication.class, args);
    }
}
