package cern.ais.pm.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.AbstractTransactionManagementConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement(proxyTargetClass = false)
public class TransactionManagementConfiguration extends AbstractTransactionManagementConfiguration {
}
