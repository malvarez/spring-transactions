package cern.ais.pm.domain;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Objects;

@Entity
public class Person {

    @Id
    @GeneratedValue
    private BigInteger id;
    private String name;
    @OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Account account;
    @Version
    private Integer version;

    public Person() {
    }

    public Person(final String name, final Account account) {
        this.name = name;
        this.account = account;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(id, person.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
