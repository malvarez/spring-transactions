package cern.ais.pm.controller;

import cern.ais.pm.command.AccountCommand;
import cern.ais.pm.exception.InsufficientFoundsException;
import cern.ais.pm.service.AccountService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/transaction")
public class TransactionController {

    private final AccountService accountService;

    public TransactionController(final AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView index() {
        Map<String, Object> model = new HashMap<>();
        model.put("accounts", accountService.getAccounts());
        return new ModelAndView("transaction", model);
    }

    @PostMapping(value = "/deposit", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String deposit(final AccountCommand command, final RedirectAttributes redirectAttributes) {
        accountService.deposit(command.getTo(), command.getAmount());
        redirectAttributes.addFlashAttribute("infoMessage", "Deposit done!");
        return "redirect:/transaction";
    }

    @PostMapping(value = "/withdraw", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String withdraw(final AccountCommand command, final RedirectAttributes redirectAttributes) {
        accountService.withdraw(command.getFrom(), command.getAmount());
        redirectAttributes.addFlashAttribute("infoMessage", "Withdraw done!");
        return "redirect:/transaction";
    }

    @PostMapping(value = "/transfer", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String transfer(final AccountCommand command, final RedirectAttributes redirectAttributes) {
        accountService.transfer(command.getFrom(), command.getTo(), command.getAmount());
        redirectAttributes.addFlashAttribute("infoMessage", "Transfer done!");
        return "redirect:/transaction";
    }

    @ExceptionHandler(InsufficientFoundsException.class)
    public String handleInsufficientFoundsException(final RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("errorMessage", "Not enough founds!");
        return "redirect:/transaction";
    }

}
