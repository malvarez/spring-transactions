package cern.ais.pm.controller;

import cern.ais.pm.service.IProxyService;
import cern.ais.pm.service.ProxyService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/proxy")
public class ProxyController {

    private IProxyService proxyService;

    public ProxyController(final IProxyService proxyService) {
        this.proxyService = proxyService;
    }

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView index() {
        Map<String, Object> model = new HashMap<>();
        Map<String, Object> properties = new LinkedHashMap<>();
        properties.put("proxyService.sayHello()", proxyService.sayHello());
        properties.put("proxyService.getClass().getName()", proxyService.getClass());
        properties.put("IProxyService.class.isInstance(proxyService)", IProxyService.class.isInstance(proxyService));
        properties.put("ProxyService.class.isInstance(proxyService)", ProxyService.class.isInstance(proxyService));
        model.put("properties", properties);
        return new ModelAndView("proxy", model);
    }
}
