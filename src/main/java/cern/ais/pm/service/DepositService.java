package cern.ais.pm.service;

import cern.ais.pm.domain.Person;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.math.BigInteger;

@Service
public class DepositService {

    private final EntityManager entityManager;

    public DepositService(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public void deposit(final BigInteger to, final BigDecimal amount) {
        Person target = entityManager.find(Person.class, to);
        BigDecimal newBalance = target.getAccount().getBalance().add(amount);
        target.getAccount().setBalance(newBalance);
    }
}
