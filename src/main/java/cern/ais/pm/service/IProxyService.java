package cern.ais.pm.service;

public interface IProxyService {

    String sayHello();
}
