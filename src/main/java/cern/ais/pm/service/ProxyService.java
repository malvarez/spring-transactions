package cern.ais.pm.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProxyService implements IProxyService {

    @Override
    @Transactional(readOnly = true)
    public String sayHello() {
        return "Hello";
    }
}
