package cern.ais.pm.service;

import cern.ais.pm.domain.Person;
import cern.ais.pm.exception.InsufficientFoundsException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.math.BigInteger;

@Service
public class WithdrawService {

    private final EntityManager entityManager;

    public WithdrawService(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public void withdraw(final BigInteger from, final BigDecimal amount) {
        Person target = entityManager.find(Person.class, from);
        BigDecimal newBalance = target.getAccount().getBalance().subtract(amount);
        if (newBalance.compareTo(BigDecimal.ZERO) < 0) {
            throw new InsufficientFoundsException();
        }
        target.getAccount().setBalance(newBalance);
    }
}
