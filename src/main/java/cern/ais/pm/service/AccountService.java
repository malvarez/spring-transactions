package cern.ais.pm.service;

import cern.ais.pm.domain.Person;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Service
public class AccountService {

    private final EntityManager entityManager;
    private final WithdrawService withdrawService;
    private final DepositService depositService;

    public AccountService(
            final EntityManager entityManager,
            final WithdrawService withdrawService,
            final DepositService depositService
    ) {
        this.entityManager = entityManager;
        this.withdrawService = withdrawService;
        this.depositService = depositService;
    }

    @Transactional(readOnly = true)
    public List<Person> getAccounts() {
        return entityManager.createQuery("select p from Person p", Person.class).getResultList();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deposit(final BigInteger from, final BigDecimal amount) {
        depositService.deposit(from, amount);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void withdraw(final BigInteger from, final BigDecimal amount) {
        withdrawService.withdraw(from, amount);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void transfer(final BigInteger from, final BigInteger to, final BigDecimal amount) {
        depositService.deposit(to, amount);
        withdrawService.withdraw(from, amount);
    }
}
